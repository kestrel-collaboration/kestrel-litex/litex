#
# This file is part of LiteX.
#
# Copyright (c) 2019 Florent Kermarrec <florent@enjoy-digital.fr>
# Copyright (c) 2019 Benjamin Herrenschmidt <benh@ozlabs.org>
# Copyright (c) 2020 - 2022 Raptor Engineering <sales@raptorengineering.com>
# SPDX-License-Identifier: BSD-2-Clause

import os

from migen import *

from litex import get_data_mod
from litex.soc.interconnect import wishbone
from litex.soc.interconnect.csr import *
from litex.gen.common import reverse_bytes
from litex.soc.cores.cpu import CPU

CPU_VARIANTS = ["standard", "standard+irq"]

class LibreSoC(CPU):
    name                 = "libresoc"
    human_name           = "LibreSoC"
    variants             = CPU_VARIANTS
    data_width           = 64
    endianness           = "little"
    gcc_triple           = ("powerpc64le-linux", "powerpc64le-linux-gnu")
    linker_output_format = "elf64-powerpcle"
    nop                  = "nop"
    io_regions           = {0xc0000000: 0x10000000} # origin, length

    @property
    def mem_map(self):
        return {
            "csr":      0xc0000000,
            "xicsicp":  0xc3ff0000,
            "xicsics":  0xc3ff1000
        }

    @property
    def gcc_flags(self):
        flags  = "-m64 "
        flags += "-mabi=elfv2 "
        flags += "-msoft-float "
        flags += "-mno-string "
        flags += "-mno-multiple "
        flags += "-mno-vsx "
        flags += "-mno-altivec "
        flags += "-mlittle-endian "
        flags += "-mstrict-align "
        flags += "-fno-stack-protector "
        flags += "-mcmodel=small "
        flags += "-D__microwatt__ "
        flags += "-D__libresoc__ "
        return flags

    def __init__(self, platform, variant="standard"):
        self.platform     = platform
        self.variant      = variant
        self.reset        = Signal()
        self.wb_insn      = wb_insn = wishbone.Interface(data_width=64, adr_width=29)
        self.wb_data      = wb_data = wishbone.Interface(data_width=64, adr_width=29)
        self.periph_buses = [wb_insn, wb_data]
        self.memory_buses = []
        self.icp_bus    = icp_bus   = wishbone.Interface(data_width=32, adr_width=12)
        self.ics_bus    = ics_bus   = wishbone.Interface(data_width=32, adr_width=12)
        if "irq" in variant:
            self.interrupt    = Signal(16)

        # # #

        self.cpu_params = dict(
            # Clock / Reset
            i_clk                 = ClockSignal(),
            i_rst                 = ResetSignal() | self.reset,

            # Wishbone instruction bus
            i_ibus__dat_r = wb_insn.dat_r,
            i_ibus__ack   = wb_insn.ack,
            i_ibus__stall = wb_insn.cyc & ~wb_insn.ack, # No burst support

            o_ibus__adr   = wb_insn.adr,
            o_ibus__dat_w = wb_insn.dat_w,
            o_ibus__cyc   = wb_insn.cyc,
            o_ibus__stb   = wb_insn.stb,
            o_ibus__sel   = wb_insn.sel,
            o_ibus__we    = wb_insn.we,

            # Wishbone data bus
            i_dbus__dat_r = wb_data.dat_r,
            i_dbus__ack   = wb_data.ack,
            i_dbus__stall = wb_data.cyc & ~wb_data.ack, # No burst support

            o_dbus__adr   = wb_data.adr,
            o_dbus__dat_w = wb_data.dat_w,
            o_dbus__cyc   = wb_data.cyc,
            o_dbus__stb   = wb_data.stb,
            o_dbus__sel   = wb_data.sel,
            o_dbus__we    = wb_data.we,

            # Wishbone XICS ICP bus
            o_icp_wb__dat_r = icp_bus.dat_r,
            o_icp_wb__ack   = icp_bus.ack,

            i_icp_wb__adr   = icp_bus.adr,
            i_icp_wb__dat_w = icp_bus.dat_w,
            i_icp_wb__cyc   = icp_bus.cyc,
            i_icp_wb__stb   = icp_bus.stb,
            i_icp_wb__sel   = icp_bus.sel,
            i_icp_wb__we    = icp_bus.we,

            # Wishbone XICS ICS bus
            o_ics_wb__dat_r = ics_bus.dat_r,
            o_ics_wb__ack   = ics_bus.ack,

            i_ics_wb__adr   = ics_bus.adr,
            i_ics_wb__dat_w = ics_bus.dat_w,
            i_ics_wb__cyc   = ics_bus.cyc,
            i_ics_wb__stb   = ics_bus.stb,
            i_ics_wb__sel   = ics_bus.sel,
            i_ics_wb__we    = ics_bus.we,

            # Debug bus
            i_dmi_addr            = 0,
            i_dmi_din             = 0,
            #o_dmi_dout           =,
            i_dmi_req             = 0,
            i_dmi_wr              = 0,
            #o_dmi_ack            =,

            # Core configuration signals
            i_core_bigendian_i    = 0,

            # Core override / debug signals
            i_msr_i_ok            = 0,

            # Interrupt controller
            i_int_level_i         = self.interrupt,
        )

        # add vhdl sources
        self.add_sources(platform, self.variant)

    def set_reset_address(self, reset_address):
        assert not hasattr(self, "reset_address")
        self.reset_address = reset_address
        assert reset_address == 0x00000000

    def add_soc_components(self, soc, soc_region_cls):
        if "irq" in self.variant:
            xicsicp_region = soc_region_cls(origin=soc.mem_map.get("xicsicp"), size=4096, cached=False)
            xicsics_region = soc_region_cls(origin=soc.mem_map.get("xicsics"), size=4096, cached=False)
            soc.bus.add_slave(name="xicsicp", slave=self.icp_bus, region=xicsicp_region)
            soc.bus.add_slave(name="xicsics", slave=self.ics_bus, region=xicsics_region)

    @staticmethod
    def add_sources(platform, variant="standard"):
        sources = [
            # External LibreSoC core
            "external_core_top.v",
        ]
        sdir = get_data_mod("cpu", "libresoc").data_location
        cdir = os.path.dirname(__file__)
        platform.add_sources(sdir, *sources)

    def do_finalize(self):
        self.specials += Instance("test_issuer", **self.cpu_params)
